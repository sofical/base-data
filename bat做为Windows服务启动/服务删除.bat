@echo off
net.exe session 1>NUL 2>NUL || (
	echo 请以管理员身份运行脚本
	goto end
)

set /p web_installed=确定需要卸载 yourServiceName 吗？(y/n)：

if %web_installed% neq y (
	goto end
)

net stop yourServiceName
sc delete yourServiceName

echo 卸载已完成，请手动重启电脑，完成残余服务清理。

:end
pause