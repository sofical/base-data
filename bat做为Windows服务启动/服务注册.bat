@echo off
net.exe session 1>NUL 2>NUL || (
	echo 请以管理员身份运行脚本
	goto end
)

set "disk=%~d0"
%disk%

set "root_dir=%~dp0"
echo 安装目录为：%root_dir%

rem 注册服务
sc create yourServiceName binPath=%root_dir%startBat.exe start=auto

rem 启动服务
net start yourServiceName

:end
pause