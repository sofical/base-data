原理：通过将 startBat.exe 注册为Windows服务，然后由 startBat.exe 去调用start.bat，实现普通bat脚本以服务形式开机启动

操作步骤：
1、在“start.bat”中编写您的开机启动脚本。
2、修改“服务注册.bat”，将“yourServiceName”替换成您的服务名称。
3、以管理员身份运行“服务注册.bat”。