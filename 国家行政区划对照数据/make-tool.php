<?php
/**
 * run code: D:\RunService\phpstudy_pro\Extensions\php\php7.3.4nts\php.exe ./make-tool.php
 */
function _buildJavaNameCodeMap() {
    $source = file_get_contents('./GB+T_2260-2007-name-code-map.json');
    $arrSource = json_decode($source, true);

    $strJavaContent = "public class DictRegionNameCode {\n";
    $strJavaContent .= "    final public static Map<String, String> nameCodeMap = new HashMap<String, String>() {{\n";
    foreach ($arrSource as $name=>$val) {
        $strJavaContent.= "        this.put(\"{$name}\", \"{$val}\");\n";
    }
    $strJavaContent .= "    }};\n";
    $strJavaContent .= "}";

    echo $strJavaContent;
}

_buildJavaNameCodeMap();